package soal2;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class SelisihTgl {

	public static void main(String[] args) throws ParseException {
		// TODO Auto-generated method stub

		Scanner s = new Scanner(System.in);
		System.out.print("tgl1 (yyyy/mm/dd) :");
		String tgl1 = s.nextLine();
		System.out.print("tgl2 (yyyy/mm/dd) :");
		String tgl2 = s.nextLine();

		SelisihTgl st = new SelisihTgl();
		System.out.println("selisih hari :" + st.hitung(tgl1, tgl2) + " hari");

	}

	public long hitung(String tgl1, String tgl2) throws ParseException {
		long selisih = 0;
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		Date tgla = (Date)format.parse(tgl1);
		Date tglb = (Date)format.parse(tgl2);

		selisih = tgla.getTime() - tglb.getTime();

		// convert milisecon ke hari
		selisih = TimeUnit.MILLISECONDS.toDays(selisih);
		return selisih;
	}

}
