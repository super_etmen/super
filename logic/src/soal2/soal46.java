package soal2;

import java.util.Scanner;

public class soal46 {

	public static void main(String[] args) {
		//soal 46
//		terdapat sebuah variabel dengan isi "abcdefghijklmnopqrstuvwxyz"
//		buat method pengecekan apakah string trsebut mengandung semua abjad
//		ex
//		out
//		kata=abcdefghijklmnopqrstuvwxyz
//		kata diatas mengandung semua abjad
//		
//		out
//		kata=aabbbbbbbcccdseea
//		kata diatas tidak mengandung semua abjad
		
		String kata="abcdefghijklmnopqrstuvwxyz";
		
	
		soal46 pg=new soal46();
		boolean hasil=pg.cek(kata);
		pg.cetak(hasil);
		
				
	}
	
	public boolean cek(String kata) {
		boolean hasil=true;
		for(char ch='a';ch<='z';ch++) {
			if(kata.indexOf(ch)<0) {
				hasil=false;
			}
		}
		return hasil;
	}
	
	public void cetak(boolean hsl) {
		if(hsl==true) {
			System.out.println("kata diatas mengandung semua abjad");
		}
		else {
			System.out.println("kata diatas tidak mengandung semua abjad");
		}
	}

}
