package soal2;

import java.util.Scanner;

public class soal50 {

	public static void main(String[] args) {
	//  soal 50
	//  di inputkan sebuah kata dan bilangan (1-10)
	//  maka kata tadi akan di mundurkan sebanyak bilangan.
	//  (hanya dari a sampai z)
	//  example 1 :
	//  input :
	//  kata = abcd
	//  bilangan = 1
	//  output : kataMundur=zabc
	//  
	//  example 2 :
	//  input : 
	//  kata = malam
	//  bilangan = 1
	//  output : kataMundur=lzkzl
		
		Scanner s=new Scanner(System.in);
		
		System.out.print("Masukan kata= ");
		String kata=s.nextLine();
		System.out.print("mundur= ");
		int mundur=s.nextInt();
		
		char huruf;
		String nyimpenHuruf="";
		
		for (int i=0;i<kata.length();i++) {
			 huruf=kata.charAt(i);
			 huruf=(char) (huruf-mundur);
			 if (huruf<'a') {
				 huruf=(char) (huruf-'a'+'z'+1);
			 }
				 
			nyimpenHuruf=nyimpenHuruf+huruf;
			
		}
		
		System.out.println("katanya jadi gini deh - "+nyimpenHuruf);

	}

}
