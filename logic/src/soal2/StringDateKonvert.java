package soal2;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class StringDateKonvert {

	public static void main(String[] args) throws ParseException {
		// TODO Auto-generated method stub
		Scanner s=new Scanner(System.in);
		System.out.print("tanggal & jam (yyyy/mm/dd hh:mm) :");
		String tgl=s.nextLine();
		
		StringDateKonvert dk= new StringDateKonvert();
		dk.konvert(tgl);
		

	}
	
	public void konvert(String tgl) throws ParseException{
		SimpleDateFormat format=new SimpleDateFormat("yyyy/MM/dd HH:mm");
		Date date=format.parse(tgl);
		String tanggal = new SimpleDateFormat("d").format(date);
		String bulan = new SimpleDateFormat("MMMM").format(date); //MMM=jan, MMMM=januari
		String tahun = new SimpleDateFormat("yyyy").format(date);
		String jam = new SimpleDateFormat("HH").format(date);
		String menit = new SimpleDateFormat("m").format(date);
		
		System.out.println("Tanggal "+tanggal+" "+bulan+" "+tahun+" Jam "+jam+" lewat "+menit);
		
	}

}
